# Module: sqs


############## REQUIRED ##################

variable "queue_name" {
  description = "Queue Name"
}

variable "fifo" {
  description = "Fifo queue enabled (true or false)"
}

variable "dead_letter_queue" {
  description = "Dead letter queue uses (true or false)"
}

variable "stack" {
  description = "Stack name or id"
}

variable "ssm_parameter_path" {
  description = "ssm parameter path"
}

variable "ssm_parameter_key" {
  description = "ssm parameter key"
}

############## OPTIONAL ##################

variable "max_receive_count" {
  description = "DLQ Maximum receives count"
  default     = 5
}

variable "delay_seconds" {
  description = "Queue delay seconds"
  default     = 90
}

variable "max_message_size" {
  description = "Queue max message size"
  default     = 2048
}

variable "message_retention_seconds" {
  description = "Queue message retention seconds"
  default     = 86400
}

variable "receive_wait_time_seconds" {
  description = "Queue receive wait time seconds"
  default     = 10
}

variable "fifo_content_based_deduplication" {
  description = "fifo queue content based deduplication"
  default     = "true"
}

variable "kms_key_reuse_seconds" {
  description = "The length of time, in seconds, for which Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again"
  default     = 86400
}

variable "visibility_timeout_seconds" {
  description = "The visibility timeout for the queue. An integer from 0 to 43200 (12 hours)."
  default = 30
}

variable "tags" {
  description = "A map of custom tags to apply to SQS Queue."
  type        = map(string)
  default     = {}
}

variable "sqs_tags" {
  description = "A map of custom tags to apply to SQS Queue."
  type        = map(string)
  default     = {}
}

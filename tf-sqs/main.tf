# Module:  sqs

locals {
  queue_name = join("-", [var.stack, var.queue_name, "queue"])
}

resource "aws_kms_key" "sqs_key" {
  description         = "KMS key for RDS Secret encryption"
  enable_key_rotation = true

}

resource "aws_sqs_queue" "terraform_queue" {
  count                             = var.fifo ? 0 : 1
  name                              = local.queue_name
  delay_seconds                     = var.delay_seconds
  max_message_size                  = var.max_message_size
  message_retention_seconds         = var.message_retention_seconds
  receive_wait_time_seconds         = var.receive_wait_time_seconds
  kms_master_key_id                 = aws_kms_key.sqs_key.id
  kms_data_key_reuse_period_seconds = var.kms_key_reuse_seconds
  visibility_timeout_seconds        = var.visibility_timeout_seconds
  redrive_policy = var.dead_letter_queue ? jsonencode({
    deadLetterTargetArn = var.dead_letter_queue ? aws_sqs_queue.queue_deadletter[count.index].arn : ""
    maxReceiveCount     = var.max_receive_count
  }) : ""

  tags = merge(
    {
      "Name" = local.queue_name,
    },
    var.tags,
    var.sqs_tags,
  )
}

resource "aws_sqs_queue" "queue_deadletter" {
  count                             = var.dead_letter_queue ? 1 : 0
  fifo_queue                        = var.fifo ? true : false
  name                              = var.fifo ? join("-", [local.queue_name, "dlq.fifo"]) : join("-", [local.queue_name, "dlq"])
  kms_master_key_id                 = aws_kms_key.sqs_key.id
  kms_data_key_reuse_period_seconds = var.kms_key_reuse_seconds
  delay_seconds                     = var.delay_seconds
  max_message_size                  = var.max_message_size
  message_retention_seconds         = var.message_retention_seconds
  receive_wait_time_seconds         = var.receive_wait_time_seconds
  visibility_timeout_seconds        = var.visibility_timeout_seconds

  tags = merge(
    {
      "Name" = join("-", [local.queue_name, "dlq"]),
    },
    var.tags,
    var.sqs_tags,
  )
}

resource "aws_sqs_queue" "terraform_queue_fifo" {
  count                             = var.fifo ? 1 : 0
  name                              = join(".", [local.queue_name, "fifo"])
  fifo_queue                        = true
  delay_seconds                     = var.delay_seconds
  max_message_size                  = var.max_message_size
  message_retention_seconds         = var.message_retention_seconds
  receive_wait_time_seconds         = var.receive_wait_time_seconds
  kms_master_key_id                 = aws_kms_key.sqs_key.id
  kms_data_key_reuse_period_seconds = var.kms_key_reuse_seconds
  visibility_timeout_seconds        = var.visibility_timeout_seconds
  content_based_deduplication       = var.fifo_content_based_deduplication
  redrive_policy = var.dead_letter_queue ? jsonencode({
    deadLetterTargetArn = var.dead_letter_queue ? aws_sqs_queue.queue_deadletter[count.index].arn : ""
    maxReceiveCount     = var.max_receive_count
  }) : ""
  tags = merge(
    {
      "Name" = join(".", [local.queue_name, "fifo"]),
    },
    var.tags,
    var.sqs_tags,
  )
}

resource "aws_ssm_parameter" "sqs_parameter" {
  name  = "/${var.ssm_parameter_path}/${var.ssm_parameter_key}"
  type  = "String"
  value = var.fifo ? aws_sqs_queue.terraform_queue_fifo[0].url : aws_sqs_queue.terraform_queue[0].url
  overwrite = true
}

SQS Module
===========

Terraform module to set up a SQS queue


Module Input Variables
----------------------

Required:

- `stack` - Stack Name/ID
- `queue_name` - Queue name
- `fifo` - Define if the queue must be a fifo queue (true/false)
- `dead_letter_queue` - Define if a deadletter queue will be needed (true/false)
- `ssm_parameter_path` - ssm parameter path
- `ssm_parameter_key` - ssm parameter key 


Optional:

- `max_receive_count` - DLQ Maximum receives count (default: 4)
- `delay_seconds` - Queue delay seconds (default: 90)
- `max_message_size` - Queue max message size (default: 2048)
- `message_retention_seconds` - Queue message retention seconds (default: 86400)
- `receive_wait_time_seconds` - Queue receive wait time seconds (default: 10)
- `visibility_timeout_seconds` -The visibility timeout for the queue. An integer from 0 to 43200 (default: 30)
- `fifo_content_based_deduplication` - fifo queue content based deduplication (default: true)
- `kms_key_reuse_seconds` - The length of time, in seconds, for which Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again (default: 86400)
- `tags` - Module tags (default: empty)
- `sqs_tags` - SQS tags (default: empty)


Usage example
-----

```hcl
module "sqs" {
  source                  = "git::ssh://git@bitbucket.org/lego-comsearch/lego-tf-modules.git//tf-sqs"
  stack = "lego"
  enabled_dlq = "true"
  fifo = "true"
  queue_name = "testqueue"
  tags = {}
  rds_tags = {}
}
```

Outputs
=======


## pre-commit

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

**Original Creator:**  Jairo Martinez

**Support:**  devops

**Support_email:**  jairo.martinez@ctg.com


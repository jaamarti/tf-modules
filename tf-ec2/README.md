EC2 module
===========

A terraform module to provide a frontened in AWS


Module Input Variables
----------------------

- `stack`                           - Name of stack
- `env`                             - Environment defined for this resources
- `ami_id`                          - Last ami defined in launch template
- `name`                            - Launch template name
- `userdata`                        - User data file
- `target_group_protocol`           - ALB Target group protocol
- `alb_listener_arn`                - Alb listener arn
- `alb_listener_rule_path_pattern`  - Alb listener rule path pattern
- `health_check_path_pattern`       - health check path pattern
- `subnet_cidr`                     - The CIDR blocks you want to allows access to instance


Optional:

- `vpcid`                         - vpc id value (default: data.aws_vpc.selected.id)
- `instance_type`                 - Instance type defined in ASG (default: "t2.micro")
- `port`                          - ALB Target group port (default: 80)
- `desired_cap`                   - Desired amount of instance in ASG (default: 1)
- `min_size`                      - The minium amount of instances in the ASGs (default: 1)
- `max_size`                      - The maximum amount of instances in the ASG (default: 1)
- `path`                          - the path for the target group (default: "")
- `additional_security_group_ids` - Add additional security groups to the ALB (default: [])
- `tags`                          - Module tags (default: empty)
- `vpc_tags`                      - vpc tags (default: empty)
- `extra_tags`                    - Extra tags (default: empty)


Usage
-----

```hcl
data "aws_lb" "selected" {
  name = "<put_here_alb_name>"
}

data "aws_lb_listener" "selected" {
  load_balancer_arn = data.aws_lb.selected.arn
  port              = 443
}

module "frontend" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/lego-tf-modules.git//tf-ec2"

  port                           = 443
  stack                          = "lego"
  target_group_protocol          = "HTTPS"
  ami_id                         = "ami-0efa459edb0ac1b41"
  userdata                       = base64encode(file("userdata.sh"))
  name                           = "frontend"
  instance_type                  = "t3.micro"
  desired_cap                    = "1"
  max_size                       = "6"
  alb_listener_rule_path_pattern = "/"
  health_check_path_pattern      = "/"
  alb_listener_arn               = data.aws_lb_listener.selected.arn
  subnet_cidr                    = ["0.0.0.0/0"]
  ec2_tags                       = { 
    "application"                = "frontend", 
    "environment"                = "lego" 
    "container.service"          = "frontend-frontend.service"
    "name"                       = "lego frontend (asg)"
    "Billing"                    = "CTG"
  }
}
```

Outputs
=======

 - `target_group_arn`             - Target group arn



Pre-commit
==========

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

# Module: ec2

locals {
  name = join("-", [var.stack, var.name])
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["${var.stack}-${var.env}"]
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:internal"
    values = ["true"]
  }
}

resource "aws_launch_template" "lego" {
  ebs_optimized = true
  image_id      = var.ami_id
  instance_type = var.instance_type
  name_prefix   = "${local.name}-"
  user_data     = var.userdata
  vpc_security_group_ids = concat(
    [aws_security_group.ec2.id],
    var.additional_security_group_ids,
  )
  monitoring {
    enabled = true
  }
  tag_specifications {
    resource_type = "instance"
    tags = merge(
      {
        "Name"    = local.name,
        "service" = var.name,
        "stack"   = "${var.stack}-${data.aws_vpc.selected.id}"
      },
      var.tags,
      var.ec2_tags,
    )
  }
}

resource "aws_autoscaling_group" "asg_template" {

  name                = local.name
  vpc_zone_identifier = data.aws_subnet_ids.selected.ids
  min_size            = var.min_size
  max_size            = var.max_size
  desired_capacity    = var.desired_cap
  target_group_arns   = [aws_lb_target_group.alb_priv_listener_tg.arn]

  launch_template {
    id      = aws_launch_template.lego.id
    version = "$Latest"
  }
  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = local.name
        "propagate_at_launch" = false
      },
      {
        "key"                 = "stack"
        "value"               = var.stack
        "propagate_at_launch" = false
      },
      {
        "key"                 = "service"
        "value"               = var.name
        "propagate_at_launch" = false
      },
    ],
    var.extra_tags,
  )

}

resource "aws_lb_target_group" "alb_priv_listener_tg" {
  name     = local.name
  port     = var.port
  protocol = var.target_group_protocol
  vpc_id   = data.aws_vpc.selected.id
  health_check {
    path                = var.health_check_path_pattern
    port                = var.port
    protocol            = var.target_group_protocol
    healthy_threshold   = 6
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200" # has to be HTTP 200 or fails
  }
  tags = merge(
    {
      "Name"    = local.name,
      "service" = var.name,
      "stack"   = "${var.stack}-${data.aws_vpc.selected.id}"
    },
    var.tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_rule" "static" {
  listener_arn = var.alb_listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_priv_listener_tg.arn
  }

  condition {
    path_pattern {
      values = [var.alb_listener_rule_path_pattern]
    }
  }
  tags = merge(
    {
      "Name"    = local.name,
      "service" = var.name,
      "stack"   = "${var.stack}-${data.aws_vpc.selected.id}"
    },
    var.tags,
  )
}

resource "aws_security_group" "ec2" {
  name        = local.name
  description = "EC2 SG created by Terraform"
  vpc_id      = data.aws_vpc.selected.id
  tags = merge(
    {
      "Name"    = local.name,
      "service" = var.name,
      "stack"   = "${var.stack}-${data.aws_vpc.selected.id}"
    },
    var.tags,
    #var.ec2_tags,
  )
}

resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2.id
  description       = "Outbound access for launch template SG"
}

resource "aws_security_group_rule" "inbound" {
  type              = "ingress"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  cidr_blocks       = var.subnet_cidr
  security_group_id = aws_security_group.ec2.id
  description       = "inbound access for launch template SG"
}


# Module: ec2


##################### REQUIRED #####################

variable "stack" {
  description = "Stack/ID defined for deployment"
}

variable "env" {
  description = "Environment defined for all module resources"
}

variable "ami_id" {
  description = "Last ami defined in launch template"
}

variable "name" {
  description = "Launch template name"
}

variable "userdata" {
  description = "User data file"
}

variable "target_group_protocol" {
  description = "ALB Target group protocol"
  type        = string
}

variable "alb_listener_arn" {
  description = "Alb listener arn"
  type        = string
}

variable "alb_listener_rule_path_pattern" {
  description = "alb listener rule path pattern"
  type        = string
}

variable "health_check_path_pattern" {
  description = "health check path pattern"
  type        = string
}

variable "subnet_cidr" {
  description = "The CIDR blocks you want to allows access to instance"
}


##################### OPTIONAL #####################

variable "vpcid" {
  description = "vpc id value"
  default     = "data.aws_vpc.selected.id"
}

variable "instance_type" {
  description = "Instance type defined in ASG"
  default     = "t2.micro"
}

variable "port" {
  description = "ALB Target group port"
  type        = number
  default     = "80"
}

variable "desired_cap" {
  description = "Desired amount of instance in ASG."
  default     = "1"
}

variable "min_size" {
  description = "The minium amount of instances in the ASG."
  default     = "1"
}

variable "max_size" {
  description = "The maximum amount of instances in the ASG."
  default     = "1"
}

variable "path" {
  description = "the path for the target group"
  default     = ""
}

variable "additional_security_group_ids" {
  description = "Add additional security groups to the ALB"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "Module tags"
  default     = {}
}

variable "extra_tags" {
  description = "Extra tags"
  default     = []
}

variable "ec2_tags" {
  description = "ec2 tags"
  default     = {}
}

ALB Module
===========

This Terraform Module creates an Application Load Balancer resources.


Module Input Variables
----------------------

Required:

- `vpc_id`          - VPC id value
- `stack`           - Stack Name/ID
- `alb_subnet_ids`  - Database instance multi az, true or false
- `env`             - Environment defined for all resources


Optional:

- `subnet_cidr`                     - Subnets ids list associated with alb (default: [])
- `enable_alb_access_logs`          - Set to true to enable the ALB to log all requests (default: false)
- `alb_access_logs_s3_bucket_name`  - Bucket name to store access logs (default: "")
- `drop_invalid_header_fields`      - If true, the ALB will drop invalid headers. Elastic Load Balancing requires that message header names contain only alphanumeric characters and hyphens (default: false)
- `enable_deletion_protection`      - If true, deletion of the ALB will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer (default: false)
- `idle_timeout`                    -The time in seconds that the client TCP connection to the ALB is allowed to be idle before the ALB closes the TCP connection (default: 60)
- `is_internal`                     - If the ALB should only accept traffic from within the VPC, set this to true. If it should accept traffic from the public Internet, set it to false (default: false)
- `additional_security_group_ids`   - "Add additional security groups to the ALB (default: [])
- `listener_port`                   - listener port that will be listening in ALB (default: 8080)
- `listener_protocol`               - protocol the listen uses (default: HTTP)
- `acm_arn`                         - ALB ACM Certificate arn (default: "")
- `tags`                            - Module tags (default: {})
- `alb_tags`                        - alb tags (default: {})


Usage example
-----

```hcl

data "aws_acm_certificate" "public_cert" {        #### ---> Only needed for public alb
  domain   = "*.dev.connect.comsearch.com"
  statuses = ["ISSUED"]
}

module "alb_public" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/lego-tf-modules.git//tf-alb"
  #source = "../../../../../lego-tf-modules/tf-alb"

  stack          = "lego"
  env            = "dev"
  vpc_id         = module.vpc.vpc_id
  alb_subnet_ids = module.vpc.public_subnets
  acm_arn        = data.aws_acm_certificate.public_cert.arn
  tags           = {
    stack = "lego-${module.vpc.vpc_id}"
  }
}

module "alb_private" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/lego-tf-modules.git//tf-alb"
  stack          = "lego"
  env            = "dev"
  vpc_id         = module.vpc.vpc_id
  alb_subnet_ids = module.vpc.private_subnets
  subnet_cidr    = module.vpc.private_subnets_cidr
  is_internal    = true
  tags           = {
    stack = "lego-${module.vpc.vpc_id}"
  }
}

```

Outputs
=======

- `alb_dns_name` - ALB dns name


Pre-commits
=======

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```
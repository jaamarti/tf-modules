# Module: alb

# Required:

variable "vpc_id" {
  description = "VPC id value"
}

variable "alb_subnet_ids" {
  description = "Subnets ids list associated with alb"
}

variable "env" {
  description = "Environment defined for all resources"
}

variable "stack" {
  description = "Stack Name/ID"
}

# Optional:

variable "subnet_cidr" {
  description = "Subnets ids list associated with alb"
  default     = []
}

variable "enable_alb_access_logs" {
  description = "Set to true to enable the ALB to log all requests. Ideally, this variable wouldn't be necessary, but because Terraform can't interpolate dynamic variables in counts, we must explicitly include this. Enter true or false."
  type        = bool
  default     = false
}

variable "alb_access_logs_s3_bucket_name" {
  description = "Bucket name to store access logs"
  type        = string
  default     = ""
}

variable "drop_invalid_header_fields" {
  description = "If true, the ALB will drop invalid headers. Elastic Load Balancing requires that message header names contain only alphanumeric characters and hyphens."
  type        = bool
  default     = false
}

variable "enable_deletion_protection" {
  description = "If true, deletion of the ALB will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer."
  type        = bool
  default     = false
}

variable "idle_timeout" {
  description = "The time in seconds that the client TCP connection to the ALB is allowed to be idle before the ALB closes the TCP connection."
  type        = number
  default     = 60
}

variable "is_internal" {
  description = "If the ALB should only accept traffic from within the VPC, set this to true. If it should accept traffic from the public Internet, set it to false."
  default     = false
}

variable "additional_security_group_ids" {
  description = "Add additional security groups to the ALB"
  type        = list(string)
  default     = []
}

variable "listener_port" {
  description = "listener port that will be listening in ALB"
  default     = "8080"
}

variable "listener_protocol" {
  description = "protocol the listen uses"
  default     = "HTTP"
}

variable "acm_arn" {
  description = "ALB ACM Certificate arn"
  default     = ""
}

variable "tags" {
  default = {}
}

variable "alb_tags" {
  default = {}
}


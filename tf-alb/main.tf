# Module: alb

locals {
  alb_name_prefix = join("-", [var.stack, var.env, "alb"])
  alb_name        = var.is_internal ? join("-", [local.alb_name_prefix, "private"]) : join("-", [local.alb_name_prefix, "public"])
}

resource "aws_alb" "alb" {
  name     = local.alb_name
  internal = var.is_internal
  subnets  = var.alb_subnet_ids
  security_groups = concat(
    [aws_security_group.alb.id],
    var.additional_security_group_ids,
  )

  idle_timeout               = var.idle_timeout
  enable_deletion_protection = var.enable_deletion_protection
  drop_invalid_header_fields = var.drop_invalid_header_fields

  tags = merge(
    {
      "Name"     = local.alb_name,
      "internal" = var.is_internal
    },
    var.tags,
    var.alb_tags,
  )

  dynamic "access_logs" {
    # The contents of the list is irrelevant. The only important thing is whether or not to create this block.
    for_each = var.enable_alb_access_logs ? ["use_access_logs"] : []
    content {
      bucket  = var.alb_access_logs_s3_bucket_name
      prefix  = local.alb_name
      enabled = true
    }
  }
}

data "aws_s3_bucket" "selected" {
  count  = var.enable_alb_access_logs ? 1 : 0
  bucket = var.alb_access_logs_s3_bucket_name
}

resource "aws_s3_bucket_policy" "default" {
  count  = var.enable_alb_access_logs ? 1 : 0
  bucket = data.aws_s3_bucket.selected[count.index].id
  policy = data.aws_iam_policy_document.default[count.index].json
}

data "aws_caller_identity" "current" {}
data "aws_elb_service_account" "default" {}

data "aws_iam_policy_document" "default" {
  count = var.enable_alb_access_logs ? 1 : 0
  statement {
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = [data.aws_elb_service_account.default.arn]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${var.alb_access_logs_s3_bucket_name}/${local.alb_name}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
    ]
  }
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${var.alb_access_logs_s3_bucket_name}/${local.alb_name}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
    ]
    condition {
      test = "StringEquals"
      values = [
        "bucket-owner-full-control"
      ]
      variable = "s3:x-amz-acl"
    }
  }
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }
    actions = [
      "s3:GetBucketAcl",
    ]
    resources = [
      "arn:aws:s3:::${var.alb_access_logs_s3_bucket_name}",
    ]
  }
}

resource "aws_lb_listener" "alb_redirect" {
  count             = var.is_internal ? 0 : 1
  load_balancer_arn = aws_alb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  tags = merge(
    {
      "Name" = join("-", [local.alb_name, "listener"])
    },
    var.tags,
    var.alb_tags,
  )
}

resource "aws_lb_listener" "alb_default" {
  load_balancer_arn = aws_alb.alb.arn
  port              = var.is_internal ? var.listener_port : 443
  protocol          = var.is_internal ? "HTTP" : "HTTPS"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      status_code  = "404"
    }
  }
  tags = merge(
    {
      "Name" = join("-", [local.alb_name, "listener"])
    },
    var.tags,
    var.alb_tags,
  )

  certificate_arn = var.is_internal ? null : var.acm_arn

}


resource "aws_security_group" "alb" {
  name        = join("-", [local.alb_name, "sg"])
  description = "ALB SG"
  vpc_id      = var.vpc_id
  tags = merge(
    {
      "Name" = join("-", [local.alb_name, "sg"])
    },
    var.tags,
    var.alb_tags,
  )
}

resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "pub_http_listeners" {
  count             = var.is_internal ? 0 : 1
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "pub_https_listeners" {
  count             = var.is_internal ? 0 : 1
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "priv_http_listeners" {
  count     = var.is_internal ? 1 : 0
  type      = "ingress"
  from_port = var.listener_port
  to_port   = var.listener_port
  protocol  = "TCP"
  self      = true
  # cidr_blocks       = var.subnet_cidr
  security_group_id = aws_security_group.alb.id
}

VPC module
===========

A terraform module to provide a VPC in AWS


Module Input Variables
----------------------

- `cidr`      - VPC cidr block
- `az_count`  - The amount of AZs we want to use for the VPC
- `stack`     - Name of stack
- `env`       - Environment defined for this resources


Optional:

- `tags`      - Module tags (default: empty)
- `vpc_tags`  - vpc tags (default: empty)


Usage
-----

```hcl
module "vpc" {
  source   = "git::ssh://git@bitbucket.org/lego-comsearch/lego-tf-modules.git//tf-vpc"
  env      = "prod"
  cidr     = "172.16.0.0/16"
  az_count = 2
}
```

Outputs
=======

 - `vpc_id`                   - outputs VPC id
 - `private_subnets`          - outputs list of private subnets
 - `public_subnets`           - outputs list of public subnets
 - `private_subnets_cidr`     - List of CIDRs of private subnets
 - `public_subnets_cidr`      - List of CIDRs of public subnets
 - `public_route_table_ids`   - List of IDs of public route tables
 - `private_route_table_ids`  - List of IDs of private route tables


Pre-commit
==========

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

Authors
=======

Chris Plimmer 
christopher.plimmer@ctg.com
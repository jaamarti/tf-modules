locals {
  resource_name = join("-", [var.stack, var.env])
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = merge(
    {
      "Name" = local.resource_name,
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_eip" "eip" {
  count = var.az_count
  vpc   = true
  tags = merge(
    {
      "Name"  = join("-", [local.resource_name, element(data.aws_availability_zones.available.names, count.index)]),
      "stack" = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    {
      "Name"  = join("-", [local.resource_name, ])
      "stack" = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_nat_gateway" "ng" {
  count         = var.az_count
  allocation_id = element(aws_eip.eip.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  depends_on    = [aws_internet_gateway.ig]
  tags = merge(
    {
      "Name"  = join("-", [local.resource_name, element(data.aws_availability_zones.available.names, count.index)])
      "stack" = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    {
      "Name"  = join("-", [local.resource_name, "public"])
      "stack" = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_route" "public_ig" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ig.id

  timeouts {
    create = "5m"
  }
}

resource "aws_subnet" "public" {
  count = var.az_count

  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index)
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = false
  tags = merge(
    {
      "Name"     = join("-", [local.resource_name, "public", element(data.aws_availability_zones.available.names, count.index)])
      "stack"    = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
      "internal" = "false"
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_route_table_association" "public" {
  count          = var.az_count
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}


resource "aws_route_table" "private" {
  count  = var.az_count
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    {
      "Name"  = join("-", [local.resource_name, "private", element(data.aws_availability_zones.available.names, count.index)])
      "stack" = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_subnet" "private" {
  count = var.az_count

  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, sum([10, count.index]))
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = false
  tags = merge(
    {
      "Name"     = join("-", [local.resource_name, "private", element(data.aws_availability_zones.available.names, count.index)])
      "stack"    = join("-", [local.resource_name, aws_vpc.vpc.id, ]),
      "internal" = "true"
    },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_route" "private_nat_gateway" {
  count = var.az_count

  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.ng.*.id, count.index)

  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "private" {
  count = var.az_count

  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_flow_log" "vpc" {
  log_destination = aws_cloudwatch_log_group.vpc.arn
  iam_role_arn    = aws_iam_role.vpc.arn
  vpc_id          = aws_vpc.vpc.id
  traffic_type    = "ALL"
}


resource "aws_kms_key" "cloudwatch_log" {
  description         = "KMS key CloudWatch Log Groups encryption"
  enable_key_rotation = true
  policy              = <<EOF
{
  "Version" : "2012-10-17",
  "Id" : "key-default-1",
  "Statement" : [ {
      "Sid" : "Enable IAM User Permissions",
      "Effect" : "Allow",
      "Principal" : {
        "AWS" : "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      },
      "Action" : "kms:*",
      "Resource" : "*"
    },
    {
      "Effect": "Allow",
      "Principal": { "Service": "logs.${data.aws_region.current.name}.amazonaws.com" },
      "Action": [ 
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*"
    }  
  ]
}
EOF
}

resource "aws_kms_alias" "alias" {
  name          = "alias/${local.resource_name}-log-group"
  target_key_id = aws_kms_key.cloudwatch_log.key_id
}

resource "aws_cloudwatch_log_group" "vpc" {
  name              = "vpc-flow-logs-${local.resource_name}"
  retention_in_days = 90
  kms_key_id        = aws_kms_key.cloudwatch_log.arn
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "role_policy" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role" "vpc" {
  name               = "vpc-flow-logs-role-${local.resource_name}"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role_policy" "vpc" {
  name   = "vpc-flow-logs-role-policy-${local.resource_name}"
  role   = aws_iam_role.vpc.id
  policy = data.aws_iam_policy_document.role_policy.json
}

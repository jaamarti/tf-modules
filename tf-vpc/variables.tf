
##################### REQUIRED #####################

variable "cidr" {
  description = "The CIDR block for the VPC"
}

variable "az_count" {
  description = "The amount of AZs we want to use for the VPC"
}

variable "stack" {
  description = "Name of stack"
}

variable "env" {
  description = "Environment defined for this resources"
}

##################### OPTIONAL #####################

variable "tags" {
  default = {}
}

variable "vpc_tags" {
  default = {}
}

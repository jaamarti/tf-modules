# Module: RDS

locals {
  name = join("-", [var.stack, var.env, var.instance_name])
}

resource "random_string" "random" {
  length = 6
  lower  = true
}

resource "random_password" "db_password" {
  length  = 16
  special = false
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = [join("-", [var.stack, var.env])]
  }
}

data "aws_subnet_ids" "private_db" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:internal"
    values = ["true"]
  }
}

data "aws_subnet" "private_db" {
  for_each = data.aws_subnet_ids.private_db.ids
  id       = each.value
}


resource "aws_db_subnet_group" "db" {
  #count = var.create_subnet_group ? 1 : 0
  description = "db subnet group for ${var.stack} rds instance"
  name_prefix = join("-", [var.stack, "rds", "subnet", "group"])
  subnet_ids  = data.aws_subnet_ids.private_db.ids
  tags = merge(
    {
      "Name" = local.name
    },
    var.tags,
    var.rds_tags,
  )
}

resource "aws_security_group" "db" {
  name_prefix = local.name
  description = "Security group for ${var.stack} rds instance"
  vpc_id      = data.aws_vpc.selected.id
  tags = merge(
    {
      "Name" = local.name,
    },
    var.tags,
    var.rds_tags,
  )
}

resource "aws_security_group_rule" "inbound_rule" {
  type      = "ingress"
  from_port = var.port
  to_port   = var.port
  protocol  = "tcp"
  #cidr_blocks       = ["data.aws_subnet.private_db.*.cidr_block"]
  cidr_blocks       = [for s in data.aws_subnet.private_db : s.cidr_block]
  security_group_id = aws_security_group.db.id
  description       = "Inbound rules added to allow connection to RDS Database"
}


resource "aws_iam_role" "enhanced_monitoring_role" {

  count = var.monitoring_interval > 0 && var.monitoring_role_arn == null ? 1 : 0

  name               = join("-", [local.name, "Monitoring-role"])
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring_role.json
  tags = merge(
    {
      "Name" = join("-", [local.name, "Monitoring_role"])
    },
    var.tags,
    var.rds_tags,
  )
}

data "aws_iam_policy_document" "enhanced_monitoring_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "enhanced_monitoring_role_attachment" {
  count      = var.monitoring_interval > 0 && var.monitoring_role_arn == null ? 1 : 0
  depends_on = [aws_iam_role.enhanced_monitoring_role]
  role = element(
    concat(aws_iam_role.enhanced_monitoring_role.*.name, [""]),
    0,
  )
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

resource "aws_db_instance" "db" {

  # Do they want a database automatically created normally I leave this out 
  name                                = var.db_name
  identifier                          = local.name
  engine                              = var.engine
  engine_version                      = var.engine_version
  instance_class                      = var.instance_type
  storage_type                        = var.storage_type
  iops                                = var.iops
  allocated_storage                   = var.allocated_storage
  storage_encrypted                   = true
  max_allocated_storage               = var.max_allocated_storage
  port                                = var.port
  vpc_security_group_ids              = [aws_security_group.db.id]
  db_subnet_group_name                = aws_db_subnet_group.db.name
  username                            = var.snapshot_identifier != null ? null : var.db_username
  password                            = var.snapshot_identifier != null ? null : random_password.db_password.result
  iam_database_authentication_enabled = var.iam_auth_enabled
  backup_retention_period             = var.backup_retention_period
  backup_window                       = var.backup_window
  final_snapshot_identifier           = "${var.stack}-rds-final-snapshot-${replace(timestamp(), ":", "-")}"
  snapshot_identifier                 = var.snapshot_identifier
  maintenance_window                  = var.maintenance_window
  skip_final_snapshot                 = var.skip_final_snapshot
  monitoring_interval                 = var.monitoring_interval
  monitoring_role_arn                 = var.monitoring_role_arn != null ? var.monitoring_role_arn : element(concat(aws_iam_role.enhanced_monitoring_role.*.arn, [""]), 0)
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports
  apply_immediately                   = var.apply_immediately
  multi_az                            = var.multi_az
  tags = merge(
    {
      "Name" = local.name
    },
    var.tags,
    var.rds_tags,
  )

  copy_tags_to_snapshot = true
  deletion_protection   = var.deletion_protection
  depends_on            = [aws_db_subnet_group.db]

  lifecycle {
    ignore_changes = [snapshot_identifier, final_snapshot_identifier]
  }
}

resource "aws_ssm_parameter" "db" {
  name      = "/${var.env}/db/${var.stack}"
  type      = "String"
  value     = "jdbc:${var.engine}://${aws_db_instance.db.endpoint}:${var.port}/${var.db_name}"
  overwrite = true
}

resource "aws_ssm_parameter" "db_hostname" {
  name      = "/${var.env}/db/${var.stack}/host"
  type      = "String"
  value     = aws_db_instance.db.endpoint
  overwrite = true
}

resource "aws_ssm_parameter" "db_port" {
  name      = "/${var.env}/db/${var.stack}/port"
  type      = "String"
  value     = var.port
  overwrite = true
}

resource "aws_ssm_parameter" "db_user" {
  name      = "/${var.env}/db/${var.stack}/user"
  type      = "String"
  value     = var.db_username
  overwrite = true
}

resource "aws_ssm_parameter" "db_user_pass" {
  name      = "/${var.stack}/${var.env}/db/${var.stack}/user/${var.db_username}"
  type      = "SecureString"
  value     = random_password.db_password.result
  overwrite = true
}


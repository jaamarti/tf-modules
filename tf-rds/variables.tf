# Module: RDS

######################### REQUIRED ######################################

variable "stack" {
  description = "Stack defined for deployment"
}

variable "env" {
  description = "Enviroment defined for deployment"
}

variable "instance_name" {
  description = "Instance name. Must be unique in this region. Must be a lowercase string."
}

variable "engine" {
  description = "database engine"
}

variable "engine_version" {
  description = "database engine version"
}

variable "allocated_storage" {
  description = "storage to allocate for rds instance"
}

variable "multi_az" {
  description = "Database instance multi az, true or false"
}

variable "instance_type" {
  description = "Database instance class"
}

variable "storage_type" {
  description = "Database storage type.  gp2 (general purpose SSD), or io1 (provisioned IOPS SSD)"
}

variable "iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of io1"
}

variable "db_username" {
  description = "database instance username"
}

########################## OPTIONAL ################################

variable "secret_recovery_window" {
  description = " Number of days that AWS Secrets Manager waits before it can delete the secret"
  default     = 0
}

variable "port" {
  description = "port used for database instance"
  default     = 5432
}

variable "max_allocated_storage" {
  description = "When configured, the upper limit to which Amazon RDS can automatically scale the storage of the DB instance. Must be greater than or equal to allocated_storage or 0 to disable Storage Autoscaling"
  default     = 0
}


variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted"
  default     = "false"
}

variable "backup_retention_period" {
  description = "Retention period in days to retain snapshots"
  default     = 7
}

variable "deletion_protection" {
  description = "Database instance delete protection"
  default     = false
}

variable "iam_auth_enabled" {
  description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled"
  default     = false
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. Valid Values: 0, 1, 5, 10, 15, 30, 60. Enhanced Monitoring metrics are useful when you want to see how different processes or threads on a DB instance use the CPU."
  type        = number
  default     = 0
}

variable "monitoring_role_arn" {
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. If monitoring_interval is greater than 0, but monitoring_role_arn is let as an empty string, a default IAM role that allows enhanced monitoring will be created."
  type        = string
  default     = null
}

variable "db_name" {
  description = "The name for your database of up to 8 alpha-numeric characters. If you do not provide a name, Amazon RDS will not create a database in the DB cluster you are creating."
  default     = ""
}

variable "snapshot_identifier" {
  description = "If non-null, the RDS Instance will be restored from the given Snapshot ID. This is the Snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05."
  type        = string
  default     = null
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL) and upgrade (PostgreSQL)."
  type        = list(string)
  default     = []
}

variable "maintenance_window" {
  description = "The weekly day and time range during which system maintenance can occur (e.g. wed:04:00-wed:04:30). Time zone is UTC. Performance may be degraded or there may even be a downtime during maintenance windows."
  type        = string
  default     = "sun:07:00-sun:08:00"
}

variable "backup_window" {
  description = "The daily time range during which automated backups are created (e.g. 04:00-09:00). Time zone is UTC. Performance may be degraded while a backup runs."
  type        = string
  default     = "06:00-07:00"
}

variable "apply_immediately" {
  description = "Specifies whether any cluster modifications are applied immediately, or during the next maintenance window. Note that cluster modifications may cause degraded performance or downtime."
  type        = bool
  default     = false
}

variable "tags" {
  default = {}
}

variable "rds_tags" {
  default = {}
}

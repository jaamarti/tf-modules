# Live: frontend

provider "aws" {
  region = "us-east-2"
}

data "aws_region" "current" {}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["VPC-DEMO-DEV"]
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Tier"
    values = ["demo-public"]
  }
}


module "front-end" {
  source        = "../../../modules/tf-ec2"
  aws_region    = "us-east-2"
  env           = "dev"
  elb_name      = "comseach-frontend-demo-dev"
  asg_max       = "4"
  asg_min       = "3"
  asg_desired   = "3"
  subnets       = data.aws_subnet_ids.selected.ids
  vpcid         = data.aws_vpc.selected.id
  instance_type = "t2.micro"
  userdata      = "../../../modules/tf-ec2/userdata.sh"
}


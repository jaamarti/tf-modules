aws_ec2_instances.where(tags: /"Name"=>"web-asg"/).instance_ids.each do |id|
    describe aws_ec2_instance(id) do
      #it              { should be_running }
      it              { should have_roles }
      its('tags_hash') { should include('env' => "dev") }
    end
  end
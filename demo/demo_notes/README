1. Create folders live, modules, global

mkdir global
mkdir modules
mkdir live

2. Run "cookiecutter ssh://git-codecommit.us-east-1.amazonaws.com/v1/repos/cookiecutter" to create the following modules:
- /modules/ec2
- /global/vpc

3. Run "cookiecutter ssh://git-codecommit.us-east-1.amazonaws.com/v1/repos/cookiecutter-live" to create the following modules:
- /live/frontend

3. put the following code into /global/vpc/main.tf:

---------------------------------------------

provider "aws" {
  region = "us-east-2"
}

data "aws_region" "current" {}

module "vpc" {

  ## Terraform Managed module ##
  source = "terraform-aws-modules/vpc/aws"

  name                = var.vpc_name
  cidr                = var.vpc_cidr
  azs		      = var.azs
  private_subnets     = var.private_subnets
  public_subnets      = var.public_subnets

  enable_nat_gateway = true
  enable_vpn_gateway = true

  public_subnet_tags = {
    Tier = "demo-public"
  }

  tags = {
    Terraform   = "true"
    Environment = "dev"
    Description = "vpc Comsearch Demo"
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = module.vpc.vpc_id
  service_name = "com.amazonaws.${data.aws_region.current.name}.s3"
}

---------------------------------------------------------------------


4. put the following code into /global/vpc/variables.tf:

------------------------------------------------------------


variable "vpc_name" {
  default = "VPC-DEMO-DEV"
  type    = string
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
  type    = string
}

variable "azs" {
  default = ["us-east-2a", "us-east-2b", "us-east-2c"]
  type    = list(any)
}

variable "private_subnets" {
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  type    = list(any)
}

variable "public_subnets" {
  default = ["10.0.100.0/24", "10.0.101.0/24", "10.0.102.0/24"]
  type    = list(any)
}


----------------------------------------------------------------------

5. Make our first commit, but first run the following command, and then make the git commit:

pre-commit install



5. Run the followings command from the folder /global/vpc:

terraform init
terraform apply

5. put the following code into /modules/ec2/main.tf:

-------------------------------------------------------------


provider "aws" {
  region = var.aws_region
}

resource "aws_elb" "web-elb" {
  name = var.elb_name
  # The same availability zone as our instances
  subnets = var.subnets
  #availability_zones = "${split(",", var.availability_zones)}"
  security_groups = ["${aws_security_group.default.id}"]
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
}
resource "aws_autoscaling_group" "web-asg" {
  #availability_zones   = "${split(",", var.availability_zones)}"
  name                 = "frontend AG"
  max_size             = var.asg_max
  min_size             = var.asg_min
  desired_capacity     = var.asg_desired
  force_delete         = true
  launch_configuration = aws_launch_configuration.web-lc.name
  load_balancers       = ["${aws_elb.web-elb.name}"]
  vpc_zone_identifier  = var.subnets

  #  tag {
  #    key                 = "Name"
  #    value               = "web-asg"
  #    propagate_at_launch = "true"
  #  }
  tags = [
    {
      key                 = "env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "web-asg"
      propagate_at_launch = true
    },
  ]

}
resource "aws_launch_configuration" "web-lc" {
  name          = "launch configuration comsearch frontend"
  image_id      = lookup(var.aws_amis, var.aws_region)
  instance_type = var.instance_type
  # Security group
  security_groups = ["${aws_security_group.default.id}"]
  user_data       = file("${var.userdata}")
  #  vpc_classic_link_id = "vpc-012a610d664dc9305"
  #  key_name        = var.key_name
}
# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "front end instances sg"
  vpc_id      = var.vpcid
  description = "Used in the terraform"
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

---------------------------------------------

6. put the following code into /modules/ec2/variables.tf:

-------------------------------------------------------------

variable "aws_amis" {
  default = {
    "us-east-2" = "ami-00399ec92321828f5"
    "us-west-1" = "ami-0d382e80be7ffdae5"
    "us-west-2" = "ami-03d5c68bab01f3496"
  }
}

variable "aws_region" {
  default = "us-east-2"
}

variable "elb_name" {
  description = "elb name for comsearch demo"
}

variable "availability_zones" {
  default     = "us-east-2a,us-east-2b,us-east-2c"
  description = "List of availability zones, use AWS CLI to find your "
}

variable "instance_type" {
  default     = "t2.micro"
  description = "AWS instance type"
}

variable "userdata" {
  description = "Front end script"
}

variable "subnets" {
  description = "Subnets list"
}

variable "vpcid" {
  description = "vpc id"
}

variable "asg_min" {
  description = "Min numbers of servers in ASG"
  default     = "1"
}
variable "asg_max" {
  description = "Max numbers of servers in ASG"
  default     = "2"
}
variable "asg_desired" {
  description = "Desired numbers of servers in ASG"
  default     = "1"
}

variable "env" {
  description = "Desired environment"
}

-------------------------------------------------------------



7. put the following code into /modules/ec2/output.tf:

------------------------------------------------------------


output "security_group" {
  value = aws_security_group.default.id
}

output "launch_configuration" {
  value = aws_launch_configuration.web-lc.id
}

output "asg_name" {
  value = aws_autoscaling_group.web-asg.id
}

output "elb_name" {
  value = aws_elb.web-elb.dns_name
}

--------------------------------------------------------------

8. put the following code into /modules/ec2/userdata.sh:

---------------------------------------------------------------

#!/bin/bash
apt-get update -y
apt-get install -y nginx > /tmp/nginx.log
sed -i 's/<title>Welcome to nginx!<\/title>/<title>COMSEARCH<\/title>/g' /var/www/html/index.nginx-debian.html
sed -i 's/<h1>Welcome to nginx!<\/h1>/\<h1>COMSEARCH Business Frontend<\/h1>/g' /var/www/html/index.nginx-debian.html
sed -i 's/sans-serif;/sans-serif;\n        background-color:grey;/g' /var/www/html/index.nginx-debian.html

----------------------------------------------------------------

9. put the following code into /live/frontend/service/main.tf:

------------------------------------------------------------------


provider "aws" {
  region = "us-east-2"
}

data "aws_region" "current" {}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values  = ["VPC-DEMO-DEV"]
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Tier"
    values = ["demo-public"]
  }
}


module "front-end" {
  source        = "../../../modules/tf-ec2"
  aws_region    = "us-east-2"
  env           = "dev"
  elb_name      = "comseach-frontend-demo-dev"
  asg_max       = "4"
  asg_min       = "3"
  asg_desired   = "3"
  subnets       = data.aws_subnet_ids.selected.ids
  vpcid         = data.aws_vpc.selected.id
  instance_type = "t2.micro"
  userdata      = "../../../modules/tf-ec2/userdata.sh"
}


------------------------------------------------------------------------

10. put the following code into /live/frontend/service/output.tf:

--------------------------------------------------------------------


output "security_group" {
  value = module.front-end.security_group
}

output "launch_configuration" {
  value = module.front-end.launch_configuration
}

output "asg_name" {
  value = module.front-end.asg_name
}

output "Front_end_elb_domain" {
  value = module.front-end.elb_name
}

--------------------------------------------------------------------

11. Run the following command from the folder /live/tl-frontend/service

inspec init profile --platform aws chef-inspec


12. Remove /live/tl-frontend/service/chef-inspec/controls/example.rb and create the file
/live/tl-frontend/service/chef-inspec/controls/control.rb with the following code in there:

-----------------------------------------------------------------
aws_ec2_instances.where(tags: /"Name"=>"web-asg"/).instance_ids.each do |id|
  describe aws_ec2_instance(id) do
    #it              { should be_running }
    it              { should have_roles }
    its('tags_hash') { should include('env' => "dev") }
  end
end

------------------------------------------------------------------

13. Create the folder /live/tl-frontend/service/terratest

16. Run the following commands from the folder /live/tl-frontend/service/terratest

go mod init "service"
go get github.com/gruntwork-io/terratest/modules/http-helper
go get github.com/gruntwork-io/terratest/modules/ssh@v0.35.4
go get github.com/gruntwork-io/terratest/modules/terraform@v0.35.4

14. Create the file /live/tl-frontend/service/terratest/frontend_test.go and put the following code into that file:

------------------------------------------------------------------
package test

import (
        "fmt"
        "strings"
        "testing"
        "time"
        http_helper "github.com/gruntwork-io/terratest/modules/http-helper"

        "github.com/gruntwork-io/terratest/modules/terraform"
)

func TestTerraformAwsFrontEndTest(t *testing.T) {
        t.Parallel()

        // Construct the terraform options with default retryable errors to handle the most common
        // retryable errors in terraform testing.
        terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
                // The path to where our Terraform code is located
                TerraformDir: "../",
        })

        // At the end of the test, run `terraform destroy` to clean up any resources that were created.
        defer terraform.Destroy(t, terraformOptions)

        // Run `terraform init` and `terraform apply`. Fail the test if there are any errors.
        terraform.InitAndApply(t, terraformOptions)

        // Run `terraform output` to get the IP of the instance
        elbName := terraform.Output(t, terraformOptions, "Front_end_elb_domain")
 
       // time.Sleep(100 * time.Second) 

        // Make an HTTP request to the instance and make sure we get back a 200 OK with the body "Hello, World!"
        url := fmt.Sprintf("http://%s", elbName)
        //http_helper.HttpGetWithRetry(t, url, nil, 200, "*string", 30, 5*time.Second)
        http_helper.HttpGetWithRetryWithCustomValidation(t, url, nil, 6, 25*time.Second, func(statusCode int, body string) bool {
                    isOk := statusCode == 200
                        isNginx := strings.Contains(body, "COMSEARCH")
                            return isOk && isNginx })
}

--------------------------------------------------------------

14. run the following command from the folder /live/tl-frontend/service/terratest:

go test -v -timeout 30m

15. Run terraform init and terraform apply commands

16. Run the following command from the folder /live/tl-frontend/service:

inspec exec chef-inspec -t aws://us-east-2
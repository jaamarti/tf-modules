# Module: ec2

variable "aws_amis" {
  default = {
    "us-east-2" = "ami-00399ec92321828f5"
    "us-west-1" = "ami-0d382e80be7ffdae5"
    "us-west-2" = "ami-03d5c68bab01f3496"
  }
}

variable "aws_region" {
  default = "us-east-2"
}

variable "elb_name" {
  description = "elb name for comsearch demo"
}

variable "availability_zones" {
  default     = "us-east-2a,us-east-2b,us-east-2c"
  description = "List of availability zones, use AWS CLI to find your "
}

variable "instance_type" {
  default     = "t2.micro"
  description = "AWS instance type"
}

variable "userdata" {
  description = "Front end script"
}

variable "subnets" {
  description = "Subnets list"
}

variable "vpcid" {
  description = "vpc id"
}

variable "asg_min" {
  description = "Min numbers of servers in ASG"
  default     = "1"
}
variable "asg_max" {
  description = "Max numbers of servers in ASG"
  default     = "2"
}
variable "asg_desired" {
  description = "Desired numbers of servers in ASG"
  default     = "1"
}

variable "env" {
  description = "Desired environment"
}


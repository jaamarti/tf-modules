output "table_name" {
  value       = join("", aws_dynamodb_table.table.*.name)
  description = "DynamoDB table name"
}

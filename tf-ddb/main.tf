resource "aws_dynamodb_table" "table" {
  name           = join("-", [var.stack, var.env, var.name, ])
  billing_mode   = var.billing_mode
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  hash_key       = var.hash_key
  range_key      = var.range_key

  dynamic "attribute" {
    for_each = var.attributes
    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }

  server_side_encryption {
    enabled     = var.enable_encryption
    kms_key_arn = var.server_side_encryption_kms_key_arn
  }

  point_in_time_recovery {
    enabled = var.enable_point_in_time_recovery
  }

  tags = merge(
    {
      "Name"        = join("-", [var.stack, var.env, var.name, ])
      "stack"       = var.stack
      "environment" = var.env
      "table_Name"  = var.name
    },
    var.tags,
    var.ddb_tags,
  )
}

resource "aws_ssm_parameter" "table_name" {
  name      = "/${var.stack}/${var.env}/${var.name}/table_name"
  type      = "String"
  value     = aws_dynamodb_table.table.name
  overwrite = true
  tags = merge(
    {
      "Name"        = join("-", [var.stack, var.env, var.name, ])
      "stack"       = var.stack
      "environment" = var.env
      "rable_name"  = var.name
    },
    var.tags,
    var.ddb_tags,
  )
}

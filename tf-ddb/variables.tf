variable "stack" {
  description = "Stack Name"
}

variable "env" {
  description = "Environment Name"
}

variable "name" {
  description = "Dynamo DB table name"
}

variable "billing_mode" {
  description = "Controls how to charge for read and write throughput and how to manage capacity [PROVISIONED, PAY_PER_REQUEST]"
  default     = "PROVISIONED"
}

variable "write_capacity" {
  description = "The number of write units for this table. If the billing_mode is PROVISIONED, this field is required."
  default     = 0
}

variable "read_capacity" {
  description = "The number of read units for this table. If the billing_mode is PROVISIONED, this field is required."
  default     = 0
}

variable "hash_key" {
  description = "The attribute to use as the hash (partition) key. Must also be defined as an attribute."
}

variable "range_key" {
  description = "The attribute to use as the range (sort) key. Must also be defined as an attribute."
  default     = null
}

variable "attributes" {
  description = "List of nested attribute definitions. Only required for hash_key and range_key attributes."
  type        = list(object({ name = string, type = string }))
}

variable "tags" {
  default = {}
}

variable "ddb_tags" {
  default = {}
}

variable "enable_encryption" {
  type        = bool
  default     = false
  description = "Enable DynamoDB server-side encryption"
}

variable "server_side_encryption_kms_key_arn" {
  type        = string
  default     = null
  description = "The ARN of the CMK that should be used for the AWS KMS encryption. This attribute should only be specified if the key is different from the default DynamoDB CMK, alias/aws/dynamodb."
}

variable "enable_point_in_time_recovery" {
  type        = bool
  default     = false
  description = "Enable DynamoDB point in time recovery"
}
